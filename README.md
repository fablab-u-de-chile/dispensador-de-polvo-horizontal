# Dispensador de polvo horizontal

Un dispensador de polvo fino impreso en 3D, funciona mediante un tornillo Auger que empuja el material hacia la boquilla de salida. Se incluye un programa para actuarlo en lazo abierto y lazo cerrado. No funciona con polvos de alto roce, o que se aglomeren, para estos casos puede resultar útil [ESTE](https://gitlab.com/fablab-u-de-chile/dispensador-de-polvo-v1) proyecto

<img src="/img/isopowdh.png" width="300">

## Atributos

- Flujo de x cc/s.
- Contenedor de n cc.
- Actuado por motor DC.
- Para anclar en perfiles V-Slot con pernos M5.

[VIDEO POR GRABAR](www.google.com) del sistema funcionando.

## Cómo construirlo

- [LISTADO](parts.md) de partes, piezas y herramientas. 
- [Partes CAD](parts/).
- [ELECTRÓNICA](elec.md).
- Instrucciones de ensamble.

## Trabajo futuro

- ...

### Ponte en contacto

Este actuador es parte de un [proyecto](https://gitlab.com/fablab-u-de-chile/biomixer) más grande desarrollado por el FabLab U de Chile. Puedes ver más de nuestra labor en:

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

## Licencia

Este trabajo está licenciado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png




