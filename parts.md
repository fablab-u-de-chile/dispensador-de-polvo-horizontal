# Partes y piezas

## Partes comerciales

 ITEM              | Cantidad
 ---------------------------   | ------------
 Motor DC 12V 60RPM | 1
 Perno Parker M4x20 | 2
 M3x8 perno avellanado | 2
 M3 tuerca | 1
 Prisionero M3x6 | 1
 Tubo de acrílico 45 mm DE | 1

 Se debe cortar una sección de tubo de 120 mm. Para el anclaje del dispensador se necesitan pernos M5.

## Partes impresas

Los archivos .step y .stl los puedes encontrar en la [carpeta de partes](parts/).

 ITEM                   | Cantidad
 ---------------------------   | ------------
 Tapa deslizante| 1
 SoporteMotor | 1
 Base | 1
 Tornillo Auger| 1
 Alimentador | 1
 Tapa | 1
 TuboDispPolvo | 1

## Herramientas

 ITEM |                  
 --------------------------- |  
 Sierra |
 Llave allen 3 mm |
 Llave allen 2 mm |
 Llave allen 1.5 mm |



